import { User } from "./User";
import { Place } from "./Place";


export class Reservation {

    /**
     * 
     * @param { init } id 
     * @param { init } newDate 
     * @param { string } newNumVis 
     * @param { string } newPlace 
     */
    constructor(id, newDate, newNumVis, newPlace){
        this.id = id;
        this.date = newDate;
        this.nbVisitor = newNumVis;
        this.place = newPlace;
    }
}