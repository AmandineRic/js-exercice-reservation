import { Reservation } from "./Reservation";
import { Place } from "./Place";


export class User {

    /**
     * 
     * @param { name } newName 
     * @param { int } id 
     * @param { int } age
     * @param {boolean} staff
     */
    constructor(newName, id, age, staff){
        this.id = id;
        this.name = newName;
        this.age = age;
        this.museumStaff = staff;
    }
    doReservation(){
        
    }
    
}