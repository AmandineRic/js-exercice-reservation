import { User } from "./User";
import { Reservation } from "./Reservation";

export class Place { 
    /**
     * 
     * @param {int} id 
     * @param {string} newPlace 
     * @param {int} limit 
     * @param {int} calendar 
     */
    constructor(id, newPlace, limit, calendar){
        this.id = id;
        this.placeName = newPlace;
        this.limitVisitor = limit;
        this.calendar = calendar;
       
    }
}