/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Place.js":
/*!**********************!*\
  !*** ./src/Place.js ***!
  \**********************/
/*! exports provided: Place */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Place", function() { return Place; });
/* harmony import */ var _User__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./User */ "./src/User.js");
/* harmony import */ var _Reservation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Reservation */ "./src/Reservation.js");



class Place { 
    /**
     * 
     * @param {int} id 
     * @param {string} newPlace 
     * @param {int} limit 
     * @param {int} calendar 
     */
    constructor(id, newPlace, limit, calendar){
        this.id = id;
        this.placeName = newPlace;
        this.limitVisitor = limit;
        this.calendar = calendar;
       
    }
}

/***/ }),

/***/ "./src/Reservation.js":
/*!****************************!*\
  !*** ./src/Reservation.js ***!
  \****************************/
/*! exports provided: Reservation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Reservation", function() { return Reservation; });
/* harmony import */ var _User__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./User */ "./src/User.js");
/* harmony import */ var _Place__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Place */ "./src/Place.js");




class Reservation {

    /**
     * 
     * @param { init } id 
     * @param { init } newDate 
     * @param { string } newNumVis 
     * @param { string } newPlace 
     */
    constructor(id, newDate, newNumVis, newPlace){
        this.id = id;
        this.date = newDate;
        this.nbVisitor = newNumVis;
        this.place = newPlace;
    }
}

/***/ }),

/***/ "./src/User.js":
/*!*********************!*\
  !*** ./src/User.js ***!
  \*********************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
/* harmony import */ var _Reservation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Reservation */ "./src/Reservation.js");
/* harmony import */ var _Place__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Place */ "./src/Place.js");




class User {

    /**
     * 
     * @param { name } newName 
     * @param { int } id 
     * @param { int } age
     * @param {boolean} staff
     */
    constructor(newName, id, age, staff){
        this.id = id;
        this.name = newName;
        this.age = age;
        this.museumStaff = staff;
    }
    doReservation(){
        
    }
    
}

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _User__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./User */ "./src/User.js");
/* harmony import */ var _Reservation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Reservation */ "./src/Reservation.js");
/* harmony import */ var _Place__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Place */ "./src/Place.js");






let reserv = new _Reservation__WEBPACK_IMPORTED_MODULE_1__["Reservation"]();
let user = new _User__WEBPACK_IMPORTED_MODULE_0__["User"](1, "Toto", 40, false);
let user1 = new _User__WEBPACK_IMPORTED_MODULE_0__["User"](2, "Toti", 30, false);
let user2 = new _User__WEBPACK_IMPORTED_MODULE_0__["User"](3, "Tata", 20, false);

let place = new _Place__WEBPACK_IMPORTED_MODULE_2__["Place"](1, "kkk", 200);
let place2 = new _Place__WEBPACK_IMPORTED_MODULE_2__["Place"](2, "mmm", 1000);

// let reserv = new Reservation(1, "kdlf", "jk", "kjj");
// let reserv1 = new Reservation(1, "kdlf", "jk", "kjj");
// let reserv2 = new Reservation(1, "kdlf", "jk", "kjj");
// let reserv3 = new Reservation(1, "kdlf", "jk", "kjj");
// let reserv4 = new Reservation(1, "kdlf", "jk", "kjj");



console.log(user);
console.log(user1);
console.log(user2);

console.log(place);
console.log(place2);

// console.log(reserv);
// console.log(reserv1);
// console.log(reserv2);
// console.log(reserv3);
// console.log(reserv4);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL1BsYWNlLmpzIiwid2VicGFjazovLy8uL3NyYy9SZXNlcnZhdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvVXNlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBQUE7QUFBQTtBQUE4QjtBQUNjOztBQUVyQyxhO0FBQ1A7QUFDQTtBQUNBLGVBQWUsSUFBSTtBQUNuQixlQUFlLE9BQU87QUFDdEIsZUFBZSxJQUFJO0FBQ25CLGVBQWUsSUFBSTtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxDOzs7Ozs7Ozs7Ozs7QUNsQkE7QUFBQTtBQUFBO0FBQUE7QUFBOEI7QUFDRTs7O0FBR3pCOztBQUVQO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsU0FBUztBQUN4QixlQUFlLFNBQVM7QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7Ozs7Ozs7QUNuQkE7QUFBQTtBQUFBO0FBQUE7QUFBNEM7QUFDWjs7O0FBR3pCOztBQUVQO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEIsZUFBZSxNQUFNO0FBQ3JCLGVBQWUsTUFBTTtBQUNyQixlQUFlLFFBQVE7QUFDdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxDOzs7Ozs7Ozs7Ozs7QUN2QkE7QUFBQTtBQUFBO0FBQUE7QUFBOEI7QUFDYztBQUNaOzs7O0FBSWhDLGlCQUFpQix3REFBVztBQUM1QixlQUFlLDBDQUFJO0FBQ25CLGdCQUFnQiwwQ0FBSTtBQUNwQixnQkFBZ0IsMENBQUk7O0FBRXBCLGdCQUFnQiw0Q0FBSztBQUNyQixpQkFBaUIsNENBQUs7O0FBRXRCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC5qc1wiKTtcbiIsImltcG9ydCB7IFVzZXIgfSBmcm9tIFwiLi9Vc2VyXCI7XG5pbXBvcnQgeyBSZXNlcnZhdGlvbiB9IGZyb20gXCIuL1Jlc2VydmF0aW9uXCI7XG5cbmV4cG9ydCBjbGFzcyBQbGFjZSB7IFxuICAgIC8qKlxuICAgICAqIFxuICAgICAqIEBwYXJhbSB7aW50fSBpZCBcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gbmV3UGxhY2UgXG4gICAgICogQHBhcmFtIHtpbnR9IGxpbWl0IFxuICAgICAqIEBwYXJhbSB7aW50fSBjYWxlbmRhciBcbiAgICAgKi9cbiAgICBjb25zdHJ1Y3RvcihpZCwgbmV3UGxhY2UsIGxpbWl0LCBjYWxlbmRhcil7XG4gICAgICAgIHRoaXMuaWQgPSBpZDtcbiAgICAgICAgdGhpcy5wbGFjZU5hbWUgPSBuZXdQbGFjZTtcbiAgICAgICAgdGhpcy5saW1pdFZpc2l0b3IgPSBsaW1pdDtcbiAgICAgICAgdGhpcy5jYWxlbmRhciA9IGNhbGVuZGFyO1xuICAgICAgIFxuICAgIH1cbn0iLCJpbXBvcnQgeyBVc2VyIH0gZnJvbSBcIi4vVXNlclwiO1xuaW1wb3J0IHsgUGxhY2UgfSBmcm9tIFwiLi9QbGFjZVwiO1xuXG5cbmV4cG9ydCBjbGFzcyBSZXNlcnZhdGlvbiB7XG5cbiAgICAvKipcbiAgICAgKiBcbiAgICAgKiBAcGFyYW0geyBpbml0IH0gaWQgXG4gICAgICogQHBhcmFtIHsgaW5pdCB9IG5ld0RhdGUgXG4gICAgICogQHBhcmFtIHsgc3RyaW5nIH0gbmV3TnVtVmlzIFxuICAgICAqIEBwYXJhbSB7IHN0cmluZyB9IG5ld1BsYWNlIFxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKGlkLCBuZXdEYXRlLCBuZXdOdW1WaXMsIG5ld1BsYWNlKXtcbiAgICAgICAgdGhpcy5pZCA9IGlkO1xuICAgICAgICB0aGlzLmRhdGUgPSBuZXdEYXRlO1xuICAgICAgICB0aGlzLm5iVmlzaXRvciA9IG5ld051bVZpcztcbiAgICAgICAgdGhpcy5wbGFjZSA9IG5ld1BsYWNlO1xuICAgIH1cbn0iLCJpbXBvcnQgeyBSZXNlcnZhdGlvbiB9IGZyb20gXCIuL1Jlc2VydmF0aW9uXCI7XG5pbXBvcnQgeyBQbGFjZSB9IGZyb20gXCIuL1BsYWNlXCI7XG5cblxuZXhwb3J0IGNsYXNzIFVzZXIge1xuXG4gICAgLyoqXG4gICAgICogXG4gICAgICogQHBhcmFtIHsgbmFtZSB9IG5ld05hbWUgXG4gICAgICogQHBhcmFtIHsgaW50IH0gaWQgXG4gICAgICogQHBhcmFtIHsgaW50IH0gYWdlXG4gICAgICogQHBhcmFtIHtib29sZWFufSBzdGFmZlxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKG5ld05hbWUsIGlkLCBhZ2UsIHN0YWZmKXtcbiAgICAgICAgdGhpcy5pZCA9IGlkO1xuICAgICAgICB0aGlzLm5hbWUgPSBuZXdOYW1lO1xuICAgICAgICB0aGlzLmFnZSA9IGFnZTtcbiAgICAgICAgdGhpcy5tdXNldW1TdGFmZiA9IHN0YWZmO1xuICAgIH1cbiAgICBkb1Jlc2VydmF0aW9uKCl7XG4gICAgICAgIFxuICAgIH1cbiAgICBcbn0iLCJpbXBvcnQgeyBVc2VyIH0gZnJvbSBcIi4vVXNlclwiO1xuaW1wb3J0IHsgUmVzZXJ2YXRpb24gfSBmcm9tIFwiLi9SZXNlcnZhdGlvblwiO1xuaW1wb3J0IHsgUGxhY2UgfSBmcm9tIFwiLi9QbGFjZVwiO1xuXG5cblxubGV0IHJlc2VydiA9IG5ldyBSZXNlcnZhdGlvbigpO1xubGV0IHVzZXIgPSBuZXcgVXNlcigxLCBcIlRvdG9cIiwgNDAsIGZhbHNlKTtcbmxldCB1c2VyMSA9IG5ldyBVc2VyKDIsIFwiVG90aVwiLCAzMCwgZmFsc2UpO1xubGV0IHVzZXIyID0gbmV3IFVzZXIoMywgXCJUYXRhXCIsIDIwLCBmYWxzZSk7XG5cbmxldCBwbGFjZSA9IG5ldyBQbGFjZSgxLCBcImtra1wiLCAyMDApO1xubGV0IHBsYWNlMiA9IG5ldyBQbGFjZSgyLCBcIm1tbVwiLCAxMDAwKTtcblxuLy8gbGV0IHJlc2VydiA9IG5ldyBSZXNlcnZhdGlvbigxLCBcImtkbGZcIiwgXCJqa1wiLCBcImtqalwiKTtcbi8vIGxldCByZXNlcnYxID0gbmV3IFJlc2VydmF0aW9uKDEsIFwia2RsZlwiLCBcImprXCIsIFwia2pqXCIpO1xuLy8gbGV0IHJlc2VydjIgPSBuZXcgUmVzZXJ2YXRpb24oMSwgXCJrZGxmXCIsIFwiamtcIiwgXCJrampcIik7XG4vLyBsZXQgcmVzZXJ2MyA9IG5ldyBSZXNlcnZhdGlvbigxLCBcImtkbGZcIiwgXCJqa1wiLCBcImtqalwiKTtcbi8vIGxldCByZXNlcnY0ID0gbmV3IFJlc2VydmF0aW9uKDEsIFwia2RsZlwiLCBcImprXCIsIFwia2pqXCIpO1xuXG5cblxuY29uc29sZS5sb2codXNlcik7XG5jb25zb2xlLmxvZyh1c2VyMSk7XG5jb25zb2xlLmxvZyh1c2VyMik7XG5cbmNvbnNvbGUubG9nKHBsYWNlKTtcbmNvbnNvbGUubG9nKHBsYWNlMik7XG5cbi8vIGNvbnNvbGUubG9nKHJlc2Vydik7XG4vLyBjb25zb2xlLmxvZyhyZXNlcnYxKTtcbi8vIGNvbnNvbGUubG9nKHJlc2VydjIpO1xuLy8gY29uc29sZS5sb2cocmVzZXJ2Myk7XG4vLyBjb25zb2xlLmxvZyhyZXNlcnY0KTtcbiJdLCJzb3VyY2VSb290IjoiIn0=